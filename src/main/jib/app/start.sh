#!/bin/bash -e
set -o pipefail

exec java ${JAVA_OPTS} -cp /app/resources:/app/classes:/app/libs/* com.raisin.MainApplication
